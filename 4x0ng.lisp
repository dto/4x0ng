;;; 4x0ng.lisp --- the further evolution of dto arcade action games

;; Copyright (C) 2014, 2015  David O'Toole

;; Author: David O'Toole <dto@monad>
;; Keywords: games

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public License
;; as published by the Free Software Foundation, either version 3 of
;; the License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Lesser General Public License for more details.

;; You should have received a copy of the GNU Lesser General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(in-package :4x0ng)

(defparameter *dead-zone* 0.1)

(defresource "pole.ogg" :volume 50)
(defresource "blob.ogg" :volume 80)
(defresource "cruise.wav" :volume 20)
(defresource "grow.wav" :volume 50)
(defresource "honk.wav" :volume 70)
(defresource "countdown.wav" :volume 150)
(defresource "fanfare.wav" :volume 230)
(defresource "lockon.wav" :volume 19)
(defresource "explode-1.wav" :volume 100)
(defresource "explode-2.wav" :volume 100)
(defresource "thrust.wav" :volume 55)
(defresource "creep.wav" :volume 80)
(defresource "fire-1.wav" :volume 195)
(defresource "fire-2.wav" :volume 195)
(defresource "fire-3.wav" :volume 195)
(defresource "whack1.wav" :volume 50)
(defresource "whack2.wav" :volume 50)
(defresource "whack3.wav" :volume 50)
(defresource "geiger1.wav" :volume 30)
(defresource "geiger2.wav" :volume 30)
(defresource "geiger3.wav" :volume 30)
(defresource "geiger4.wav" :volume 30)
(defresource "data-1.wav" :volume 140)
(defresource "data-2.wav" :volume 140)
(defresource "data-3.wav" :volume 140)
(defresource "reactor-1.wav" :volume 70)
(defresource "reactor-2.wav" :volume 70)
(defresource "reactor-3.wav" :volume 70)
(defresource "reactor-4.wav" :volume 70)
(defresource "reactor-5.wav" :volume 70)
(defresource "bump-1.wav" :volume 140)
(defresource "encounter.wav" :volume 140)
(defresource "rezz-1.wav" :volume 80)
(defresource "rezz-2.wav" :volume 80)
(defresource "rezz-3.wav" :volume 80)

(defparameter *geiger-sounds*
'("geiger1.wav" "geiger3.wav"))

(defparameter *reactor-sounds*
'("reactor-1.wav" "reactor-2.wav" "reactor-3.wav" "reactor-4.wav" "reactor-5.wav"))

(defun level-fanfare (n)
  (nth (mod n (length *reactor-sounds*)) *reactor-sounds*))

(defvar *message* nil)
(defvar *message-clock* 0)
(defvar *danger-clock* 0)
(defparameter *message-time* 250)
(defun show-message (text &optional (time *message-time*))
  (setf *message-clock* time)
  (setf *message* text))
(defun update-message-clock ()
  (when (plusp *danger-clock*)
    (decf *danger-clock*))
  (when (plusp *message-clock*)
    (decf *message-clock*))
  (when (zerop *message-clock*)
    (setf *message* nil)))
(defun clear-message ()
  (setf *message-clock* 0)
  (setf *message* nil))

(defvar *level* 0)
(defun level () *level*)

(defun by-level (&rest args)
  (if (<= (length args) *level*)
      (* 1.2 (nth (mod *level* (length args)) args))
      (nth *level* args)))

(defvar *danger* 0)
(defparameter *max-danger* 100)
(defun danger () *danger*)

(defun max-danger-p () 
  (<= *max-danger* (danger)))

(defun set-danger (value)
  (setf *danger* 
	(max 0 (min *max-danger* value))))

(defsetf danger set-danger)

(defun increase-danger (&optional (amount 1))
  (setf *danger-clock* 10)
  (setf (danger) (+ amount (danger))))

(defun decrease-danger (&optional (amount 1))
  (setf (danger) (- (danger) amount)))

(defun clear-danger ()
  (setf (danger) 0))

(defvar *use-music* nil)
(defvar *use-deathwalls* nil)
(defvar *use-plasma* nil)
(defvar *use-gravity* nil)
(defvar *difficult* nil)

(defvar *variation* 1)
(defparameter *variations* 
  '(()
    (:plasma) 
    (:deathwalls) 
    (:gravity) 
    (:plasma :deathwalls)
    (:gravity :deathwalls)
    (:plasma :difficult)
    (:gravity :difficult)
    (:deathwalls)
    (:plasma :gravity)
    (:plasma :deathwalls) 
    (:plasma :gravity :difficult)
    (:plasma :deathwalls :difficult :gravity)))

(defun variation-features (n)
  (let ((index (mod n (length *variations*))))
    (nth index *variations*)))

(defun configure-game (&optional (variation *variation*))
  (setf *use-plasma* nil)
  (setf *use-gravity* nil)
  (setf *use-deathwalls* nil)
  (setf *difficult* nil)
  (dolist (feature (variation-features variation))
    (case feature
      (:difficult (setf *difficult* t))
      (:gravity (setf *use-gravity* t))
      (:deathwalls (setf *use-deathwalls* t))
      (:plasma (setf *use-plasma* t)))))

(defvar *arena* nil)
(defun arena () *arena*)
(defvar *player-1* nil)
(defun player-1 () *player-1*)
(defun set-player-1 (x) (setf *player-1* x))

(defvar *joystick-enabled* t)

(defun holding-down-arrow () (or (keyboard-down-p :kp2) (keyboard-down-p :down)))
(defun holding-up-arrow () (or (keyboard-down-p :kp8) (keyboard-down-p :up)))
(defun holding-left-arrow () (or (keyboard-down-p :kp4) (keyboard-down-p :left)))
(defun holding-right-arrow () (or (keyboard-down-p :kp6) (keyboard-down-p :right)))

;;; Sound effects

(defresource "serve.wav" :volume 23)
(defresource "grab.wav" :volume 23)
(defresource "bounce.wav" :volume 10)
(defresource "newball.wav" :volume 20)
(defresource "return.wav" :volume 20)
(defresource "error.wav" :volume 80)

(defresource 
      (:name "boop1.wav" :type :sample :file "boop1.wav" :properties (:volume 20))
      (:name "boop2.wav" :type :sample :file "boop2.wav" :properties (:volume 20))
    (:name "boop3.wav" :type :sample :file "boop3.wav" :properties (:volume 20)))

(defparameter *bounce-sounds* '("boop1.wav" "boop2.wav" "boop3.wav"))

(defresource 
    (:name "doorbell1.wav" :type :sample :file "doorbell1.wav" :properties (:volume 83))
    (:name "doorbell2.wav" :type :sample :file "doorbell2.wav" :properties (:volume 83))
  (:name "doorbell3.wav" :type :sample :file "doorbell3.wav" :properties (:volume 83)))

(defparameter *doorbell-sounds* '("doorbell1.wav" "doorbell2.wav" "doorbell3.wav"))

(defparameter *slam-sounds* '("slam1.wav" "slam2.wav" "slam3.wav"))

(defresource 
    (:name "slam1.wav" :type :sample :file "slam1.wav" :properties (:volume 52))
    (:name "slam2.wav" :type :sample :file "slam2.wav" :properties (:volume 52))
  (:name "slam3.wav" :type :sample :file "slam3.wav" :properties (:volume 52)))

;; (defresource 
;;     (:name "whack1.wav" :type :sample :file "whack1.wav" :properties (:volume 12))
;;     (:name "whack2.wav" :type :sample :file "whack2.wav" :properties (:volume 12))
;;   (:name "whack3.wav" :type :sample :file "whack3.wav" :properties (:volume 12)))

(defparameter *whack-sounds* '("whack1.wav" "whack2.wav" "whack3.wav"))

(defresource 
    (:name "color1.wav" :type :sample :file "color1.wav" :properties (:volume 32))
    (:name "color2.wav" :type :sample :file "color2.wav" :properties (:volume 32))
  (:name "color3.wav" :type :sample :file "color3.wav" :properties (:volume 32)))

(defparameter *color-sounds* '("color1.wav" "color2.wav" "color3.wav"))

(defresource "analog-death.wav" :volume 120)
(defresource "hyperdeath.wav" :volume 100)

;;; Basic parameters

;; (defparameter *width* 1280)
;; (defparameter *height* 720)
(defparameter *width* 1280)
(defparameter *height* 720)
(defparameter *unit* 20)
(defun units (n) (* n *unit*))

(defparameter *player-1-color* "hot pink")
(defparameter *player-2-color* "orange")
(defparameter *neutral-color* "white")
(defparameter *arena-color* "yellow green")
(defparameter *wall-color* "gray50")

;;; Finding all objects of a given class in a buffer

(defun find-instances (buffer class-name)
  (when (typep buffer (find-class 'buffer))
    (let ((objects (objects buffer)))
      (when objects
	(loop for thing being the hash-values in objects
	      when (typep (find-object thing t) (find-class class-name))
		collect (find-object thing t))))))

;;; Game clock and scoring

(defun seconds (n) (* 60 n))
(defun minutes (n) (* (seconds 60) n))

(defparameter *game-length* (seconds 45))

(defvar *game-clock* 0)

(defun reset-game-clock ()
  (setf *game-clock* *game-length*))

(defun update-game-clock ()
  (when (plusp *game-clock*)
    (decf *game-clock*)))

(defun game-on-p () (plusp *game-clock*))

(defun game-clock () *game-clock*)

(defun game-clock-string (&optional (clock *game-clock*))
  (let ((minutes (/ clock (minutes 1)))
	(seconds (/ (rem clock (minutes 1)) 
		    (seconds 1))))
    (format nil "~D~A~2,'0D" (truncate minutes) ":" (truncate seconds))))

(defvar *score-1* 0)
(defvar *score-2* 0)

(defun reset-score () (setf *score-1* 0 *score-2* 0))

;;; Physics 

(defclass thing (xelf:node)
  ((color :initform *neutral-color*)
   (gravity :initform t)
   (inertia :initform t)
   (speed :initform 1)
   (heading :initform 0.0)
   ;; thrust magnitudes
   (tx :initform 0.0)
   (ty :initform 0.0)
   ;; physics vars
   (dx :initform 0.0)
   (dy :initform 0.0)
   (ddx :initform 0.0)
   (ddy :initform 0.0)
   ;; physics params
   (max-dx :initform 100)
   (max-dy :initform 100)
   (max-ddx :initform 4)
   (max-ddy :initform 4)))

(defmethod at-rest-p ((thing thing))
  (with-slots (dx dy) thing
    (and (> *dead-zone* (abs dx))
	 (> *dead-zone* (abs dy)))))

(defmethod slow-p ((thing thing))
  (with-slots (dx dy) thing
    (and (> 1 (abs dx))
	 (> 1 (abs dy)))))

(defmethod expand ((thing thing) &optional (factor 1.01))
  (with-slots (height width x y) thing
    (multiple-value-bind (cx cy) (center-point thing)
      (let ((new-height (* height factor))
	    (new-width (* width factor)))
	(resize thing new-width new-height)
	(move-to thing (- cx (/ new-width 2)) (- cy (/ new-height 2)))))))

(defmethod knock-toward-center ((thing thing))
  (multiple-value-bind (gx gy) (center-point thing)
    (multiple-value-bind (cx cy) (center-point (current-buffer))
      (let ((jerk-distance (/ (distance cx cy gx gy) 16)))
	(with-slots (heading) thing
	  (setf heading (find-heading gx gy cx cy))
	  (move thing heading jerk-distance))))))

(defmethod restrict-to-buffer ((thing thing))
  (unless (bounding-box-contains (multiple-value-list (bounding-box (current-buffer)))
				 (multiple-value-list (bounding-box thing)))
    (reset-physics thing)
    (knock-toward-center thing)))

(defmethod handle-collision ((u thing) (v thing))
  (collide u v)
  (collide v u))

(defmethod max-speed ((thing thing)) (slot-value thing 'max-dx))
(defmethod max-acceleration ((thing thing)) (slot-value thing 'max-ddx))

(defparameter *thrust* 0.3)

(defmethod center-of-arena ()
  (values (/ *width* 2) (/ *height* 2)))

(defmethod heading-to-center ((thing thing))
  (multiple-value-bind (tx ty) (center-point thing)
    (multiple-value-bind (cx cy) (center-of-arena)
      (find-heading tx ty cx cy))))
  
(defun clamp (x bound)
  (max (- bound)
       (min x bound)))

(defun clamp0 (x bound)
  (let ((value (clamp x bound)))
    (if (< (abs value) *dead-zone*)
	0
	value)))

(defmethod decay ((thing thing) x)
  (let ((z (* 0.94 x)))
    z))

(defmethod movement-heading ((self thing)) nil)

(defmethod current-heading ((self thing)) 
  (slot-value self 'heading))

(defmethod thrust-x ((self thing)) 
  (let ((heading (movement-heading self)))
    (when heading
      *thrust*)))
      
(defmethod thrust-y ((self thing)) 
  (let ((heading (movement-heading self)))
    (when heading
      *thrust*)))

(defmethod heading-to-core ((thing thing))
  (multiple-value-bind (x y) (center-point thing)
    (find-heading x y (/ *width* 2) (/ *height* 2))))

(defparameter *gravity-coefficient* 0.118)

(defmethod gravity-p ((thing thing)) 
  (and *use-gravity* (slot-value thing 'gravity)))

(defmethod gravity-tx ((thing thing))
  (if (not (gravity-p thing)) 0
      (* *gravity-coefficient*
	 (cos (heading-to-core thing)))))

(defmethod gravity-ty ((thing thing))
  (if (not (gravity-p thing)) 0
      (* *gravity-coefficient*
	 (sin (heading-to-core thing)))))

(defmethod update-thrust ((thing thing))
  (with-slots (tx ty) thing
    (let ((heading (current-heading thing))
	  (thrust-x (thrust-x thing))
	  (thrust-y (thrust-y thing))
	  (gx (gravity-tx thing))
	  (gy (gravity-ty thing)))
      (setf tx (if thrust-x 
		   (+ gx (* thrust-x (cos heading)))
		   (when *use-gravity* gx)))
      (setf ty (if thrust-y 
		   (+ gy (* thrust-y (sin heading)))
		   (when *use-gravity* gy))))))

;; (defmethod update-thrust ((self thing))
;;   (with-slots (tx ty) self
;;     (let ((heading (current-heading self))
;; 	  (thrust-x (thrust-x self))
;; 	  (thrust-y (thrust-y self)))
;;       (setf tx (if thrust-x (* thrust-x (cos heading)) nil))
;;       (setf ty (if thrust-y (* thrust-y (sin heading)) nil)))))

(defmethod reset-physics ((self thing))
  (with-slots (dx dy ddx ddy) self
    (setf dx 0 dy 0 ddx 0 ddy 0)))

(defmethod impel ((self thing) &key speed heading)
  (play-sample (random-choose *whack-sounds*))
  (with-slots (tx ty dx dy ddx ddy) self
    (setf (slot-value self 'heading) heading)
    (setf ddx 0 ddy 0)
    (setf dx (* speed (cos heading)))
    (setf dy (* speed (sin heading)))))
          
(defmethod repel ((this thing) (that thing) &optional (speed 10))
  (impel that :speed speed :heading (heading-between this that)))

(defmethod update-physics ((self thing))
  (with-slots (x y dx dy ddx ddy tx ty
		  max-dx max-ddx max-dy max-ddy) self
    (setf ddx (clamp (or tx (decay self ddx))
		     (max-acceleration self)))
    (setf dx (clamp (if tx (+ dx ddx) (decay self dx))
		    (max-speed self)))
    (setf ddy (clamp (or ty (decay self ddy))
		     (max-acceleration self)))
    (setf dy (clamp (if ty (+ dy ddy) (decay self dy))
		    (max-speed self)))))

(defmethod update-position ((self thing))
  (with-slots (x y dx dy) self
    (move-to self 
	     (+ x dx)
	     (+ y dy))))

(defmethod update-heading ((self thing))
  (with-slots (heading) self
    (setf heading (or (movement-heading self) heading))))

(defmethod update :before ((thing thing))
  (unless (eq :passive (slot-value thing 'collision-type))
    (update-thrust thing)
    (update-physics thing)
    (update-position thing)
    (update-heading thing)))

(defmethod draw ((self thing))
  (with-slots (color image heading) self
    (multiple-value-bind (top left right bottom)
	(bounding-box self)
      (draw-textured-rectangle-* left top 0
				 (- right left) (- bottom top)
				 (find-texture image)
				 ;; apply shading
				 :vertex-color color
				 :blend :alpha
				 ;; adjust angle to normalize for up-pointing sprites 
				 :angle (+ 90 (heading-degrees heading))))))

;;; The bouncing Squareparticle

(defparameter *particle-size* (units 0.5))
(defparameter *kick-disabled-time* 40)
(defparameter *fire-disabled-time* 20)

(defvar *particle* nil)
(defun particle () *particle*)

(defun random-serve-heading () (random (* 2 pi)))

(defun image-set (name count &optional (start 1))
  (loop for n from start to count
	collect (format nil "~A-~S.png" name n)))

(defparameter *photon-images* (image-set "photon" 3))
(defparameter *particle-images* (image-set "ball" 6))

(defun random-particle-image ()
  (random-choose *particle-images*))

(defclass particle (thing)
  ((max-dx :initform 100)
   (max-dy :initform 100)
   (max-ddx :initform 0.01)
   (max-ddy :initform 0.01)
   (inertia :initform nil)
   (speed :initform 4)
   (bounced :initform nil)
   (last-wall :initform nil)
   (image :initform (random-particle-image))
   (kick-clock :initform 0)
   (color :initform *neutral-color*)
   (heading :initform (random-serve-heading))))

(defmethod go-to ((particle particle) x y)
  (move-to particle (- x (/ *particle-size* 2)) (- y (/ *particle-size* 2))))

(defmethod initialize-instance :after ((particle particle) &key)
  (setf *particle* particle)
  (resize particle *particle-size* *particle-size*))

(defmethod paint ((particle particle) color)
  (setf (slot-value particle 'color) color))

(defmethod disable-kicking ((particle particle))
  (setf (slot-value particle 'kick-clock) *kick-disabled-time*))

(defmethod recently-kicked-p ((particle particle))
  (plusp (slot-value particle 'kick-clock)))

(defmethod bounce ((particle particle) &optional angle)
  (with-slots (heading) particle
    (reset-physics particle)
    ;; (when (or (< (abs (- heading (/ pi 2))) 0.1)
    ;; 	      (< (abs (- heading (/ (* 3 pi) 2))) 0.1))
    ;;   (setf heading (random (* pi 2))))
    (setf heading (or angle (opposite-heading heading)))
    (move particle heading 5)))
    ;; (when (object-eq particle (carrying-particle-p (player-1)))
    ;;   (setf (slot-value (player-1) 'ball) nil))))
    ;; ;; (impel particle :speed (slot-value particle 'speed) :heading heading)))

(defmethod update ((particle particle))
  (with-slots (image x y dx dy kick-clock heading speed color) particle
    (when (plusp kick-clock)
      (decf kick-clock))
    (restrict-to-buffer particle)
    (setf dx (* speed (cos heading)))
    (setf dy (* speed (sin heading)))))

;;; Event cards

(defparameter *card-font* "sans-mono-bold-16")

(defclass card (thing)
  ((text :initform "0")
   (happy :initform nil)
   (gravity :initform nil)
   (clock :initform 140)))

(defmethod initialize-instance :after ((card card) &key (text "MESSAGE") (happy nil))
  (setf (slot-value card 'text) text)
  (setf (slot-value card 'happy) happy)
  (resize card 20 20))

(defmethod update :after ((card card))
  (with-slots (clock) card
    (decf clock)
    (unless (plusp clock)
      (destroy card))))

(defmethod draw ((card card))
  (with-slots (x y text happy) card
    (draw-string text x y 
		 :color (random-choose 
			 (if happy '("yellow" "green")
			     '("red" "yellow")))
		 :font *card-font*)))

(defmethod notify ((thing thing) text &optional happy)
  (multiple-value-bind (x y) (center-point thing)
    (add-node (current-buffer) (make-instance 'card :text text :happy happy) x y)))

;;; Proton bombs

(defparameter *proton-images* (image-set "proton" 5))
(defparameter *proton-size* 14)

(defclass proton (particle)
  ((image :initform (random-choose *proton-images*))
   (bounced :initform t)
   (inertia :initform nil)
   (gravity :initform nil)
   (speed :initform 3)
   (clock :initform 60)))

(defmethod initialize-instance :after ((proton proton) &key heading speed)
  (setf (slot-value proton 'heading) heading)
  (setf (slot-value proton 'speed) 0)
  (resize proton *proton-size* *proton-size*))

(defmethod update :after ((proton proton))
  (with-slots (image clock heading) proton
    (setf image (random-choose *proton-images*))
    (incf heading 0.01)
    (if (plusp clock)
	(decf clock)
	(destroy proton))))

(defmethod destroy :before ((proton proton))
  (quarkle proton 15)
  (increase-danger 3)
  (notify proton "D++")
  (play-sample "bump-1.wav"))

(defmethod danger-level ((particle particle)) 0.2)

(defmethod bounce :after ((proton proton) &optional angle)
  (play-sample "bounce.wav"))

;;; Bubbles

(defparameter *bubble-size* 100)

(defparameter *bubble-images* (image-set "field" 2))

(defun random-bubble-image ()
  (random-choose *bubble-images*))

(defclass bubble (thing)
  ((max-dx :initform 100)
   (max-dy :initform 100)
   (max-ddx :initform 0.01)
   (max-ddy :initform 0.01)
   (inertia :initform t)
   (speed :initform 1)
   (clock :initform 2000)
   (image :initform (random-bubble-image))
   (heading :initform (random-serve-heading))))

(defmethod initialize-instance :after ((bubble bubble) &key)
  (setf (slot-value bubble 'heading) (random (* 2 pi)))
  (resize bubble *bubble-size* *bubble-size*))

(defmethod bounce ((bubble bubble) &optional (speed 20))
  (with-slots (heading) bubble
    (reset-physics bubble)
    (setf heading (- (opposite-heading heading) (* (random-choose '(-1 1)) (/ pi 2))))
    (move bubble heading 2)
    (impel bubble :speed speed :heading heading)))

(defmethod update ((bubble bubble))
  (with-slots (image x y dx dy clock heading speed color) bubble
    (when (plusp clock)
      (decf clock))
    (restrict-to-buffer bubble)
    (setf dx (* speed (cos heading)))
    (setf dy (* speed (sin heading)))
    (incf heading 0.004)
    (percent-of-time 40 (setf image (random-bubble-image)))
    (when (zerop clock)
      (destroy bubble))))
 
(defmethod draw ((self bubble))
  (with-slots (color image heading) self
    (multiple-value-bind (top left right bottom)
	(bounding-box self)
      (draw-textured-rectangle-* left top 0
				 (- right left) (- bottom top)
				 (find-texture (random-bubble-image))
				 :vertex-color (random-choose '("yellow" "magenta" "cyan"))
				 :blend :additive
				 :opacity 0.2
				 :angle (random (* 2 pi))))))

;;; Particle types

(defclass alpha (particle)
  ((image :initform "alpha.png")))

(defclass beta (particle)
  ((image :initform "beta.png")))

(defclass gamma (particle)
  ((image :initform "gamma.png")
   (speed :initform 4.85)))

(defparameter *gamma-images* '("gamma.png" "gamma2.png" "gamma3.png" "gamma4.png"))

(defmethod initialize-instance :after ((gamma gamma) &key)
  (resize gamma 18 18))

(defmethod update :after ((gamma gamma))
  (setf (slot-value gamma 'image) (random-choose *gamma-images*)))

;;; Sparkles

(defparameter *spark-images* '("rezlight1.png" "rezlight2.png" "rezlight3.png" "rezlight4.png" "rezlight5.png"))

(defun spark-size () (random-choose '(30 25 28)))

(defclass spark (thing)
  ((timer :initform (random-choose '(20 30 40 50 60)))
   (speed :initform (random-choose '(3 4 5)))
   (image :initform (random-choose *spark-images*))))

(defmethod initialize-instance :after ((spark spark) &key heading)
  (setf (field-value 'heading spark) heading)
  (let ((size (spark-size)))
    (resize spark size size)))

(defmethod update ((spark spark))
  (with-slots (timer speed heading width height) spark
    (decf timer)
    (if (not (plusp timer))
	(destroy spark)
	(progn
	  (move spark heading speed)
	  (incf speed 0.3)
	  (resize spark (- width 0.2) (- height 0.2))))))

(defmethod draw ((spark spark))
  (with-slots (x y width height heading image) spark
    (draw-textured-rectangle-* x y 0 width height (find-texture (random-choose *spark-images*)) :blend :additive)))

(defun make-sparks (x y &optional (count (random-choose '(4 5 6))))
  (dotimes (n count)
    (add-node (current-buffer) (new 'spark :heading (random (* 2 pi))) x y)))

(defmethod sparkle ((thing thing) &optional (count 6))
  (multiple-value-bind (x y) (center-point thing)
    (make-sparks x y count)))

;;; Quarkles

(defparameter *quark-images* '("chiblur-1.png" "chiblur-2.png" "chiblur-3.png" "chiblur-4.png" "chiblur-5.png"))

(defun quark-size () (random-choose '(30 25 28)))

(defclass quark (thing)
  ((timer :initform (random-choose '(55 65)))
   (speed :initform (random-choose '(3.5 4 4.5 4.8)))
   (image :initform (random-choose *quark-images*))))

(defmethod initialize-instance :after ((quark quark) &key heading)
  (setf (field-value 'heading quark) heading)
  (let ((size (quark-size)))
    (resize quark size size)))

(defmethod update ((quark quark))
  (with-slots (timer speed heading width height) quark
    (decf timer)
    (if (not (plusp timer))
	(destroy quark)
	(progn
	  (move quark heading speed)))))

(defmethod draw ((quark quark))
  (with-slots (x y width height heading image) quark
    (draw-textured-rectangle-* x y 0 width height (find-texture (random-choose *quark-images*)) :blend :additive)))

(defun make-quarks (x y &optional (count (random-choose '(4 5 6))))
  (dotimes (n count)
    (add-node (current-buffer) (new 'quark :heading (random (* 2 pi))) x y)))

(defmethod quarkle ((thing thing) &optional (count 6))
  (multiple-value-bind (x y) (center-point thing)
    (make-quarks x y count)))

;;; Trail

(defparameter *trail-length* 108)

(defparameter *trail-images* (image-set "trail" 2))

(defclass trail (thing)
  ((image :initform (random-choose *trail-images*))
   (gravity :initform nil)
   (clock :initform *trail-length*)))

(defmethod collide ((trail trail) (proton proton)) nil)

(defmethod collide ((trail trail) (bubble bubble)) 
  (percent-of-time 2
    (when (charged-p (player-1))
      (expand bubble 1.04)
      (increase-danger 1.3)
      (play-sample (random-choose '("creep.wav" "data-1.wav" "data-2.wav" "data-3.wav")))
      (notify bubble "D+"))))

(defmethod update ((trail trail))
  (resize trail 5 5)
  (with-slots (clock image) trail
    (setf image (random-choose *trail-images*))
    (decf clock)
    (unless (plusp clock)
      (destroy trail))))

(defmethod collide ((trail trail) (particle particle))
  (when (charged-p (player-1))
    (decrease-danger 10)
    (notify particle "D-" t)
    (play-sample (random-choose '("photon-1.wav" "photon-2.wav" "photon-3.wav")))
    (multiple-value-bind (x y) (center-point particle)
      (make-sparks x y 12))
    (incf *score-1*)
    (destroy particle)))

(defmethod collide ((this trail) (that trail))
  (destroy this))

(defmethod draw :after ((trail trail))
  (when (charged-p (player-1))
    (multiple-value-bind (top left right bottom)
	(bounding-box trail)
      (draw-textured-rectangle-* (- left 5) (- top 5) 0
				 15 15
				 (find-texture (random-choose '("field-1.png" "field-2.png")) )
				 :vertex-color (random-choose '("yellow" "magenta" "cyan"))
				 :blend :additive :opacity 0.8))))

;;; Walls and bricks

(defclass wall (thing)
  ((color :initform *wall-color*)
   (gravity :initform nil)
   (orientation :initform :horizontal)))

(defmethod collide :after ((beta beta) (wall wall))
  (expand beta 1.03))

(defmethod initialize-instance :after ((wall wall) &key (orientation :horizontal))
  (setf (slot-value wall 'orientation) orientation))

(defmethod find-color ((wall wall))
  (with-slots (color) wall
    (if *use-deathwalls* (random-choose '("orange red" "dark orange")) color)))

(defmethod draw ((wall wall))
  (with-slots (x y width height) wall
    (draw-box x y width height :color (find-color wall))))

(defmethod vertical-reflection ((particle particle) (wall wall))
  (with-slots (heading) particle
    (multiple-value-bind (px py) (center-point particle)
      (multiple-value-bind (x y) (center-point wall)
	(if (> px x)
	    ;; wall is left of particle
	    (if (> heading pi) ;; downward
		(- pi heading)
		;; upward
		(- pi heading))
	    ;; wall is right-of particle
	    (if (plusp heading) ;; upward
		(- pi heading)
		;; downward
		(- pi heading)))))))
 
(defmethod horizontal-reflection ((particle particle) (wall wall))
  (with-slots (heading) particle
    (multiple-value-bind (px py) (center-point particle)
      (multiple-value-bind (x y) (center-point wall)
	(if (> py y)
	    ;; wall is above particle. heading is positive
	    (- 0 heading)
	    ;; wall is below particle. heading is negative
	    (- 0 heading))))))

(defmethod danger-level ((particle particle))
  (by-level 0.3 0.32 0.34 0.345 0.35 0.355 0.357 0.36 0.363 0.365 0.367 0.371 0.375 0.379 0.381 0.384 0.392 0.4))

(defmethod collide ((particle particle) (wall wall))
  (setf (slot-value particle 'bounced) t)
  (setf (slot-value particle 'last-wall) wall)
  (notify particle "D+")
  (increase-danger (danger-level particle))
  (play-sample (random-choose *whack-sounds*))
  (restore-location particle)
  (bounce particle 
	  (case (slot-value wall 'orientation)
	    (:horizontal (horizontal-reflection particle wall))
	    (:vertical (vertical-reflection particle wall)))))

(defmethod collide :after ((gamma gamma) (wall wall))
  (multiple-value-bind (x y) (center-point gamma)
    (make-sparks x y 16))
  (play-sample "data-3.wav")
  (notify gamma "D++")
  (with-slots (x y width height) gamma
    (resize gamma (+ width 8) (+ height 8))
    (move-to gamma (- x 8) (- y 8)))
  (increase-danger (by-level 1.6 1.8 1.8 2.0 2.2 2.6 2.8)))

(defmethod collide :around ((particle particle) (wall wall))
  (with-slots (last-wall) particle
    (if (and last-wall (object-eq last-wall wall))
	(knock-toward-center particle)
	(call-next-method))))

(defmethod destroy :before ((gamma gamma))
  (notify gamma "D--" t))

(defmethod destroy :after ((gamma gamma))
  (decrease-danger 12))

(defparameter *brick-width* (units 1.8))
(defparameter *brick-height* (units 1.2))

(defclass brick (thing)
  ((collision-type :initform :passive)
   (color :initform "white")
   (height :initform *brick-height*)
   (width :initform *brick-width*)))

(defmethod handle-collision ((this brick) (that brick)) nil)

(defmethod collide ((particle particle) (brick brick))
  (destroy brick)
  (play-sample (random-choose *color-sounds*))
  (bounce particle 10)
  (percent-of-time 20 (impel particle :speed 10 :heading (random (* pi 2)))))

(defmethod draw ((brick brick))
  (with-slots (x y width height color) brick
    (draw-box x y width height :color color)))

(defun make-brick (x y &optional (color "cyan"))
  (let ((brick (make-instance 'brick)))
    (resize brick *brick-width* *brick-height*)
    (move-to brick x y)
    (setf (slot-value brick 'color) color)
    brick))

(defun make-column (x y count &optional (color "cyan"))
  (with-new-buffer
    (dotimes (n count)
      (add-node (current-buffer) (make-brick x y color) x y)
      (incf y *brick-height*))
    (current-buffer)))

(defparameter *fortress-height* 28)

(defun make-fortress (x y colors)
  (with-new-buffer 
    (dolist (color colors)
      (paste (current-buffer) (make-column x y *fortress-height* color))
      (incf x *brick-width*)
      (current-buffer))))

(defparameter *player-1-fortress-colors* '("dark orchid" "medium orchid" "orchid"))

(defparameter *player-2-fortress-colors* '("dark orange" "orange" "gold"))

;;; A player bot, either human or AI controlled

(defparameter *max-speed* 3)
(defparameter *max-carry-speed* 3.5)

(defvar *serve-period-timer* 0)
(defparameter *serve-period* 55)

(defun update-serve-period-timer ()
  (when (plusp *serve-period-timer*)
    (decf *serve-period-timer*)))

(defun serve-period-p ()
  (plusp *serve-period-timer*))

(defun begin-serve-period ()
  (setf *serve-period-timer* *serve-period*))

(defparameter *trail-timer* 3)

(defclass robot (thing)
  ((max-dx :initform *max-speed*)
   (max-dy :initform *max-speed*)
   (max-ddx :initform 1.8)
   (max-ddy :initform 1.8)
   (dead :initform nil)
   (ball :initform nil)
   (trail-clock :initform 0)
   (image :initform "lander.png")
   (color :initform "white")
   (carrying :initform nil)
   (fire-heading :initform 0)
   (fire-clock :initform 0)
   (kick-clock :initform 0)))

(defmethod smash ((robot robot))
  (when (not (slot-value (arena) 'destroyed))
    (halt-music)
    (setf (slot-value robot 'dead) t)
    (play-sample "hyperdeath.wav")
    (show-message "PROGRAM TERMINATED")
    (multiple-value-bind (x y) (center-point robot)
      (make-sparks x y 16))
    (play-sample "analog-death.wav")
    (clear-particles)
    (setf (slot-value (arena) 'destroyed) t)
    (setf (slot-value (arena) 'background-image) "meltdown.png")))

(defmethod ready-to-kick-p ((robot robot)) 
  (zerop (slot-value robot 'kick-clock)))

(defmethod ready-to-fire-p ((robot robot)) 
  (zerop (slot-value robot 'fire-clock)))

(defmethod find-score ((robot robot)) (slot-value robot 'score))

(defmethod score-point (player) nil)

(defmethod find-goal ((robot robot)) (goal-1))

(defmethod carrying-particle-p ((robot robot))
  (slot-value robot 'ball))

(defmethod max-speed ((robot robot))
  (if (carrying-particle-p robot) 
      *max-carry-speed* 
      (slot-value robot 'max-dx)))

(defun find-robots ()
  (find-instances (arena) 'robot))

(defun particle-carrier ()
  (find-if #'carrying-particle-p (find-robots)))

(defparameter *traditional-robot-colors* '("gold" "olive drab" "RoyalBlue3" "dark orchid"))

(defparameter *robot-size* 20)

(defmethod initialize-instance :after ((robot robot) &key)
  (resize robot *robot-size* *robot-size*))

(defmethod humanp ((robot robot)) nil)

(defparameter *robot-reload-frames* 50)
(defparameter *bullet-reload-frames* 20)

;;; Can't pass through walls

(defmethod collide ((wall wall) (robot robot))
  (if *use-deathwalls* (smash robot)
      (progn (increase-danger 3)
	     (notify robot "D+")
	     (impel robot :speed 10 :heading (heading-to-center robot)))))

(defmethod collide ((bubble bubble) (thing thing)) nil)

(defmethod collide ((bubble bubble) (robot robot))
  (multiple-value-bind (x y) (center-point robot)
    (multiple-value-bind (bx by) (center-point bubble)
      (when (< (distance x y bx by) (- (/ (slot-value bubble 'width) 2) 10))
	(smash robot)))))

(defmethod collide ((brick brick) (robot robot))
  (impel robot :speed 10 :heading (heading-to-center robot)))

(defmethod charged-p ((self robot))
  (and (plusp (slot-value self 'kick-clock))
       (not (carrying-particle-p self))))

;;; Apply shading/detail to the robot

(defmethod draw ((self robot))
  (with-slots (dead color heading kick-clock) self
    (when (not dead)
      (multiple-value-bind (top left right bottom)
	  (bounding-box self)
	(draw-textured-rectangle-* left top 0
				   (- right left) (- bottom top)
				   (find-texture "lander.png")
				   :vertex-color color
				   :angle (+ 90 (heading-degrees heading)))))))
	;; (when (charged-p (player-1))
	;;   (draw-textured-rectangle-* (- left (units 1))
	;; 			     (- top (units 1))
	;; 			     0
	;; 			     60 60
	;; 			     (find-texture (random-choose '("field-1.png" "field-2.png")) )
	;; 			     :vertex-color (random-choose '("yellow" "magenta" "cyan"))
	;; 			     :blend :additive :opacity 0.8))))))


(defun jitter (heading)
  (+ heading (* (if (difficult-p) 0.15 0.2) (sin (/ *updates* 24)))))

(defparameter *player-1-joystick* 0)

(defmethod stick-heading ((self robot)) 
  (when (left-analog-stick-pressed-p *player-1-joystick*)
    (left-analog-stick-heading *player-1-joystick*)))

;;; Control logic 

(defparameter *kick-speed* 28)
(defparameter *steal-speed* 32)
(defparameter *kick-range* (units 2.8))

(defmethod ball-within-range-p ((robot robot))
  (< (distance-between robot (ball))
     *kick-range*))

(defmethod kick ((self robot))
  (with-slots (ball kick-clock) self
    (increase-danger 2.2)
    (notify self "D+")
    (incf *score-1* 2)
    (play-sample (random-choose *bounce-sounds*))
    (setf kick-clock *robot-reload-frames*)
    (when ball
	(impel ball 
	       :heading (heading-between self ball)
	       :speed 10)
	(play-sample "serve.wav")
	(setf ball nil))))

(defmethod fire ((self robot))
  (with-slots (x y dx dy fire-clock fire-heading heading) self
    (setf fire-clock *bullet-reload-frames*)
    (let ((speed (+ 2 (abs (* 2 (/ (+ (abs dx) (abs dy)) 2))))))
      (if (< 2 (length (find-instances (arena) 'proton)))
	  (progn (play-sample "honk.wav")
		 (notify self "cannot fire"))
	  (multiple-value-bind (cx cy) (center-point self)
	    (increase-danger 0.2)
	    (incf *score-1* 3)
	    (play-sample (random-choose *geiger-sounds*))
	    (let ((proton (make-instance 'proton :heading fire-heading :speed speed)))
	      (add-node (current-buffer) proton
			(- cx (/ *proton-size* 2))
			(- cy (/ *proton-size* 2)))))))))
  ;; (impel proton
	    ;; 	   :heading fire-heading :speed speed)))))))
  
(defmethod collide ((this robot) (that robot))
  (repel this that))

(defparameter *derezz-images* (image-set "derezz" 4))

(defmethod update ((self robot))
  (with-slots (dead fire-heading heading fire-clock kick-clock image trail-clock) self
    (unless (firing-p self)
      (setf fire-heading heading))
    (when (plusp trail-clock)
      (decf trail-clock))
    (when (and (zerop trail-clock) 
	       (not (carrying-particle-p self)))
      (setf trail-clock *trail-timer*)
      (multiple-value-bind (x y) (center-point self)
	(when (not dead)
	  (add-node (current-buffer) (new 'trail) x y))))
    (when (plusp kick-clock)
      (decf kick-clock))
    (when (and (ready-to-kick-p self)
	       (kicking-p self))
      (kick self))
    (when (plusp fire-clock)
      (decf fire-clock))
    (when (and (ready-to-fire-p self)
	       (firing-p self))
	  (fire self))))

(defmethod update :around ((self robot))
  (if (not (slot-value self 'dead))
      (call-next-method)
      (setf (slot-value self 'image) (random-choose *derezz-images*))))

(defparameter *all-buttons* '(0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20))
(defvar *fire-button* 15)

(defun other-buttons ()
  (remove *fire-button* *all-buttons* :test 'eql))

(defmethod kicking-p ((robot robot))
  (or (holding-shift)
      (some #'(lambda (button)
		(joystick-button-pressed-p button *player-1-joystick*))
	    (other-buttons))))

(defmethod firing-p ((robot robot))
  (or (keyboard-down-p :space)
      (joystick-button-pressed-p *fire-button* *player-1-joystick*)))

(defmethod humanp ((self robot)) t)

(defun arrow-direction ()
  (cond 
    ((and (holding-down-arrow) (holding-right-arrow)) :downright)
    ((and (holding-down-arrow) (holding-left-arrow)) :downleft)
    ((and (holding-up-arrow) (holding-right-arrow)) :upright)
    ((and (holding-up-arrow) (holding-left-arrow)) :upleft)
    ((holding-down-arrow) :down)
    ((holding-up-arrow) :up)
    ((holding-left-arrow) :left)
    ((holding-right-arrow) :right)))

(defmethod thrust-x ((self robot)) 
  (destructuring-bind (horizontal vertical) (xelf::joystick-left-analog-stick)
    (when (or (member (arrow-direction) '(:left :right :upleft :downleft :upright :downright))
	      (joystick-axis-pressed-p horizontal *player-1-joystick*))
      *thrust*)))

(defmethod thrust-y ((self robot)) 
  (destructuring-bind (horizontal vertical) (xelf::joystick-left-analog-stick)
    (when (or (member (arrow-direction) '(:up :down :upleft :downleft :upright :downright))
	      (joystick-axis-pressed-p vertical *player-1-joystick*))
      *thrust*)))

(defmethod stick-heading ((self robot))
  (when (or (left-analog-stick-pressed-p *player-1-joystick*))
    (left-analog-stick-heading *player-1-joystick*)))
  
(defmethod movement-heading ((self robot))
  (let ((dir (or 
	      (arrow-direction)
	      (when *joystick-enabled*
		;; restrict to 8-way
		(when (stick-heading self)
		  (or (heading-direction (stick-heading self))
		      :left))))))
    (when dir (direction-heading dir))))

(defmethod grab ((robot robot) (ball particle))
  (when (and (not (recently-kicked-p ball))
	     (ready-to-kick-p robot))
    (play-sample (random-choose *doorbell-sounds*))
    (setf (slot-value robot 'ball) ball)))

(defmethod collide ((robot robot) (ball particle)) nil)
  ;;(unless (or (particle-carrier) (recently-kicked-p ball))
    ;; (grab robot ball)))

;;; Glue the ball to the ball carrier, with some wobble

(defun wobble () (sin (/ xelf:*updates* 10)))

(defmethod carry-location ((robot robot))
  (with-slots (heading) robot
    (multiple-value-bind (cx cy) (center-point robot)
      (multiple-value-bind (tx ty) 
	  (step-coordinates cx cy heading (units 2))
	(multiple-value-bind (wx wy)
	    (step-coordinates tx ty (- heading (/ pi 2)) (* 30 (wobble)))
	  (values (- wx (* *particle-size* 0.12))
		  (- wy (* *particle-size* 0.12))))))))

(defmethod update :after ((particle particle))
  (let ((carrier (particle-carrier)))
    (when (and carrier (object-eq particle (carrying-particle-p (player-1))))
      (multiple-value-bind (x y) 
	  (carry-location carrier)
	(go-to particle x y)))))

;;; The core

(defparameter *minimum-core-size* 212)
(defparameter *core-image-count* 75)
(defparameter *core-images* (image-set "core" *core-image-count*))

(defun core-image-* (n)
  (nth (min n (- *core-image-count* 1))
       *core-images*))

(defun core-image (danger)
  (let ((index (truncate (* danger (/ *core-image-count* *max-danger*)))))
    (core-image-* index)))

(defun core-radius (&optional (danger (danger)))
  (+ (/ *minimum-core-size* 2) (* (danger) 1.43)))

(defparameter *pulse-sounds* '("pulse-1.wav" "pulse-2.wav" "pulse-3.wav" "pulse-4.wav" "pulse-5.wav" "pulse-6.wav"))

(defresource "pulse-1.wav" :volume 50)
(defresource "pulse-2.wav" :volume 50)
(defresource "pulse-3.wav" :volume 50)
(defresource "pulse-4.wav" :volume 50)
(defresource "pulse-5.wav" :volume 50)
(defresource "pulse-6.wav" :volume 50)

(defun core-pulse (danger)
  (let ((index (truncate (* danger (/ (length *pulse-sounds*) *max-danger*)))))
    (or (nth index *pulse-sounds*) "pulse-6.wav")))

(defparameter *pulse-durations* '(100 95 90 85 80 75 70 65 60 55 50 45 40 35 30 25 20 15))

(defun pulse-duration (danger)
  (let ((index (truncate (* danger (/ (length *pulse-durations*) *max-danger*)))))
    (or (nth index *pulse-durations*) 100)))

(defclass core (thing)
  ((clock :initform 0)
   (ticks :initform 0)
   (image :initform "core-1.png")))

(defmethod draw :around ((core core))
  (when (not (meltdown-p))
    (call-next-method)))

(defmethod collide ((core core) (quark quark)) nil)

(defmethod collide ((quark quark) (core core)) nil)

(defmethod draw :after ((core core))
  (when *use-plasma*
    (multiple-value-bind (x y) (center-point core)
      (let ((r (+ (core-radius) 40)))
	(draw-textured-rectangle-*
	 (- x r) (- y r) 0 (* 2 r) (* r 2)
	 (find-texture (random-choose '("field-1.png" "field-2.png")) )
	 :vertex-color (random-choose '("yellow" "magenta" "hot pink"))
	 :blend :additive :opacity 0.92)))
    (set-blending-mode :alpha)))

(defmethod initialize-instance :after ((core core) &key)
  (resize core *minimum-core-size* *minimum-core-size*))

(defun ambient-danger-rate ()
  (by-level 0.69 0.70 0.71 0.72 0.73 0.74 0.75 0.755 0.76 0.765 0.77 0.8))

(defun time-pressure ()
  (let ((mark (/ *game-length* *game-clock*)))
    (cfloat (* 0.7 (max 0 (- 1 (/ 1 mark)))))))

(defun hazard ()
  (let ((options 
	  '((bubble) 
	    (monitor)
	    (monitor gamma) 
	    (bubble gamma)
	    (monitor tracer)
	    (monitor bubble gamma)
	    (bubble gamma base)
	    (monitor gamma)
	    (bubble monitor gamma)
	    (base monitor monitor)
	    (base monitor bubble)
	    (base monitor gamma)
	    (monitor bubble base)
	    (monitor gamma)
	    (bubble gamma)
	    (base monitor))))
    (let ((classes (nth (mod *level* (length options)) options)))
      (when classes (make-instance (random-choose classes))))))

(defmethod update ((core core))
  (with-slots (ticks) core
    (incf ticks)
    (when (member ticks '(20 60 120 180 230 420) :test 'eql)
      (percent-of-time (by-level 5 10 15 20 25 30)
	(add-random-hazard (arena)))))
  (percent-of-time (by-level 0.1600 0.1610 0.1615 0.1620 0.1625 0.1632 0.1639 0.1642 0.165)
    (add-random-hazard (arena)))
  (percent-of-time
      (+ (ambient-danger-rate)
	 (time-pressure))
    (increase-danger 0.5))
  (when (max-danger-p) 
    (meltdown (arena)))
  (update-message-clock)
  (update-game-clock)
  (when (= *game-clock* (seconds 16))
    (play-sample "countdown.wav")
    (show-message "15 SECONDS LEFT!"))
  (when (= *game-clock* (seconds 5))
    (play-sample "doorbell3.wav"))
  (when (= *game-clock* (seconds 4))
    (play-sample "doorbell3.wav"))
  (when (= *game-clock* (seconds 3))
    (play-sample "doorbell3.wav"))
  (when (= *game-clock* (seconds 2))
    (play-sample "doorbell3.wav"))
  (when (= *game-clock* (seconds 1))
    (play-sample "doorbell3.wav"))
  (when (not (game-on-p))
    (reset-game-clock)
    (incf *level*)
    (configure-game *level*)
    (clear-danger)
    (clear-particles)
    (announce *level*))
  (percent-of-time (+ (ambient-danger-rate) 0.034 (time-pressure) (/ (danger) -1100))
    (let ((particle (make-instance (random-choose '(alpha beta)))))
       (multiple-value-bind (x y) (center-point core)
	(add-node (current-buffer) particle x y)
	(increase-danger (by-level 2 2.2 2.8 3 3.2 3.7 4 4.3 4.5 4.8 5))
	(impel particle :speed 10 :heading (random (* 2 pi))))))
  (with-slots (clock image heading) core
    (incf heading (+ 0.001 (/ (danger) 10000)))
    (when (evenp clock) 
      (setf image (core-image (+ (danger) (/ (mod xelf::*updates* 5) 2)))))
    (when (zerop clock)
      (setf clock (pulse-duration (danger)))
      (play-sample (core-pulse (danger))))
    (decf clock))
  (resize core (* (core-radius) 2) (* (core-radius) 2))
  (multiple-value-bind (x y) (center-point (current-buffer))
    (move-to core (- x (core-radius)) (- y (core-radius)))))

(defmethod update :around ((core core))
  (when (not (meltdown-p))
    (call-next-method)))

(defmethod collide :before ((particle particle) (core core))
  (restore-location particle))

(defmethod collide ((thing thing) (core core))
  (repel core thing))

(defmethod collide :after ((particle particle) (core core))
  (decf (slot-value particle 'heading) (/ pi 4)))

(defmethod collide :around ((thing thing) (core core))
  (multiple-value-bind (x y) (center-point core)
    (multiple-value-bind (x0 y0) (center-point thing)
      (when (< (distance x0 y0 x y) (core-radius))
	(call-next-method)))))

(defmethod collide :after ((q quark) (c core)) (destroy q))

(defmethod collide :around ((particle particle) (core core))
  (when (slot-value particle 'bounced)
    (call-next-method)))

(defmethod collide :after ((robot robot) (core core))
  (when *use-plasma* 
      (smash robot)))
      ;; (progn 
      ;; 	(repel core robot)
      ;; 	(increase-danger 2)
      ;; 	(play-sample (random-choose '("data-3.wav" "data-2.wav" "data-1.wav")))
      ;; 	(notify robot "D++"))))

(defmethod collide :around ((robot robot) (core core))
  (when (not (slot-value robot 'dead))
    (call-next-method)))

;;; Monitor enemy

(defparameter *monitor-images* '("patrol-1.png" "patrol-2.png" "patrol-3.png"))

(defclass monitor (thing)
  ((inertia :initform nil)
   (gravity :initform nil)
   (clock :initform (by-level 1000 1100 1200 1300 1400))
   (image :initform (random-choose *monitor-images*))
   (speed :initform (by-level 0.05 0.06 0.07 0.08 0.09 0.1 0.12))))

(defmethod max-speed ((monitor monitor))
  (by-level 0.5 0.7 0.9 1.4 1.8 2 2.2 2.4))

(defmethod initialize-instance :after ((monitor monitor) &key)
  (resize monitor 32 32))

(defmethod update :before ((monitor monitor))
  (percent-of-time 2
    (with-slots (speed heading width height) monitor  
      (setf heading (heading-between monitor (player-1))))))

(defmethod update ((monitor monitor))
  (with-slots (image x y tx ty heading clock speed) monitor
    (restrict-to-buffer monitor)
    ;;(setf image (random-choose *monitor-images*))
    (setf tx (* speed (cos heading)))
    (setf ty (* speed (sin heading)))))
    ;; (decf clock)
    ;; (unless (plusp clock)
    ;;   (destroy monitor))))

(defmethod movement-heading ((monitor monitor))
  (heading-between monitor (player-1)))

(defmethod thrust-x ((monitor monitor))
  (slot-value monitor 'speed))

(defmethod thrust-y ((monitor monitor))
  (slot-value monitor 'speed))

(defmethod collide ((monitor monitor) (core core)) nil)

(defmethod collide ((monitor monitor) (wall wall))
  (when *use-deathwalls*
    (sparkle monitor)
    (destroy monitor))) 

(defmethod collide ((bubble bubble) (core core)) nil)

(defmethod collide ((proton proton) (core core)) 
  (increase-danger 4)
  (notify proton "D+")
  (destroy proton))

(defmethod destroy :before ((monitor monitor))
  (decrease-danger 5)
  (notify monitor "D++" :happy))

;; (defmethod collide ((monitor monitor) (proton proton))
;;   (multiple-value-bind (x y) (center-point monitor)
;;     (make-sparks x y 10)
;;     (play-sample "bump-1.wav")
;;     (destroy monitor)
;;     (destroy proton)))

(defmethod collide ((wall wall) (monitor monitor))
  (impel monitor :heading (heading-to-center monitor) :speed 5))

(defmethod collide ((monitor monitor) (robot robot))
  (smash robot))

(defmethod collide ((monitor monitor) (quark quark))
  (sparkle monitor)
  (destroy monitor)
  (play-sample "explode-2.wav"))

(defmethod bounce ((monitor monitor) &optional (speed 20))
  (with-slots (heading) monitor
    (reset-physics monitor)
    (setf heading (- (opposite-heading heading) (* (random-choose '(-1 1)) (/ pi 2))))
    (move monitor heading 10)
    (impel monitor :speed speed :heading heading)))

;;; Flaptors

(defclass flaptor (monitor)
  ((image :initform "yasichi.png")))

(defmethod initialize-instance :after ((flaptor flaptor) &key)
  (resize flaptor 20 20)
  (with-slots (speed) flaptor (setf speed (* 2 speed))))

;;; Tracers

(defparameter *tracer-trail-images* (image-set "bubble" 7))

(defclass neutron (thing)
  ((image :initform (random-choose *tracer-trail-images*))
   (clock :initform 170)))

(defmethod collide ((neutron neutron) (core core))
  (destroy neutron))

(defmethod collide ((neutron neutron) (wall wall))
  (destroy neutron))

(defmethod initialize-instance :after ((neutron neutron) &key)
  (resize neutron 4 4))

(defmethod update :after ((neutron neutron))
  (with-slots (clock) neutron
    (decf clock)
    (unless (plusp clock)
      (destroy neutron))))

(defmethod collide ((neutron neutron) (robot robot))
  (smash robot))

(defmethod collide ((neutron neutron) (quark quark))
  (destroy neutron)
  (destroy quark))

(defclass tracer (particle)
  ((image :initform "tracer.png")
   (clock :initform 0)
   (bounced :initform t)
   (speed :initform 1.5)))

(defmethod initialize-instance :after ((tracer tracer) &key)
  (resize tracer 30 30))

(defmethod collide ((tracer tracer) (robot robot))
  (smash robot))

(defmethod collide ((tracer tracer) (proton proton)) nil)
(defmethod collide ((tracer tracer) (trail trail)) nil)
(defmethod collide ((trail trail) (tracer tracer)) nil)

(defmethod collide ((neutron neutron) (spark spark))
  (destroy spark)
  (destroy neutron))

(defmethod update :after ((tracer tracer))
  (with-slots (clock) tracer
    (if (zerop clock)
	(multiple-value-bind (x y) (center-point tracer)
	  (setf clock 4)
	  (add-node (current-buffer) (make-instance 'neutron) (- x 4) (- y 4)))
	(decf clock))))

(defmethod collide ((tracer tracer) (quark quark))
  (sparkle tracer)
  (destroy tracer)
  (play-sample "explode-2.wav"))

;;; Bases

(defclass base (thing)
  ((shield :initform 3 :accessor shield :initarg :shield)
   (max-dx :initform 2)
   (max-dy :initform 2)
   (heading :initform (random (* 2 pi)))
   (image :initform "generator.png")))

(defmethod initialize-instance :after ((base base) &key)
  (resize base 45 45))

(defmethod collide ((base base) (wall wall))
  (knock-toward-center base))

(defmethod collide ((proton proton) (wall wall))
  (increase-danger) 
  (notify proton "D+")
  (sparkle proton)
  (destroy proton))

(defmethod collide ((proton proton) (bubble bubble))
  (expand bubble)
  (play-sample "encounter.wav")
  (destroy proton))

(defmethod collide ((base base) (quark quark))
  (multiple-value-bind (x y) (center-point base)
    (make-sparks x y 10)
    (decf (shield base))
    (destroy quark)
    (when (plusp (shield base))
      (destroy base))))

(defmethod update ((base base))
  (percent-of-time 0.3
    (multiple-value-bind (x y) (center-point base)
      (play-sample "photon.wav")
      (add-node (current-buffer) (make-instance (random-choose '(monitor tracer))) x y))))

;;; The arena and game setup

(defun make-wall (x y width height &optional (orientation :horizontal))
  (let ((wall (make-instance 'wall)))
    (xelf:resize wall width height)
    (setf (slot-value wall 'orientation) orientation)
    (xelf:move-to wall x y)
    wall))

(defun make-border (x y width height)
  (let ((left x)
	(top y)
	(right (+ x width))
	(bottom (+ y height)))
    (with-new-buffer
      ;; top wall
      (insert (make-wall left top (- right left) (units 1) :horizontal))
      ;; bottom wall
      (insert (make-wall left bottom (- right left (units -1)) (units 1) :horizontal))
      ;; left wall
      (insert (make-wall left top (units 1) (- bottom top) :vertical))
      ;; right wall
      (insert (make-wall right top (units 1) (- bottom top (units -1)) :vertical))
      ;; send it back
      (current-buffer))))

(defclass arena (xelf:buffer)
  ((resetting :initform nil)
   (ended :initform nil)
   (destroyed :initform nil)
   (quadtree-depth :initform 9)
   (background-image :initform "turf.png")))
   
(defun clear-particles ()
  (mapc #'destroy (find-instances (arena) 'particle))
  (mapc #'destroy (find-instances (arena) 'monitor))
  (mapc #'destroy (find-instances (arena) 'proton))
  (mapc #'destroy (find-instances (arena) 'neutron))
  (mapc #'destroy (find-instances (arena) 'tracer))
  (mapc #'destroy (find-instances (arena) 'base))
  (mapc #'destroy (find-instances (arena) 'bubble)))

(defmethod meltdown ((arena arena))
  (smash (first (find-robots)))
  (show-message "MELTDOWN. SIMULATION TERMINATED.")
  (play-sample "analog-death.wav")
  (play-sample "explode-1.wav")
  (setf (slot-value arena 'destroyed) t)
  (clear-particles)
  (setf (slot-value arena 'background-image) "meltdown.png"))

(defun meltdown-p ()
  (slot-value (arena) 'destroyed))

(defmethod add-node :after ((arena arena) (gamma gamma) &optional x y z)
  (play-sample "grow.wav"))

(defvar *reset-clock* nil)

(defmethod quit-game ((arena arena))
  (quit))

(defmethod initialize-instance :after ((arena arena) &key)
  (setf *arena* arena)
  (resize arena *width* *height*)
  (bind-event arena '(:r :control) 'reset-game)
  (bind-event arena '(:y :control) 'reset-level)
  (bind-event arena '(:f4) 'add-random-hazard)
  (bind-event arena '(:escape) 'setup)
  ;; (bind-event arena '(:m :control) 'toggle-music)
  (bind-event arena '(:q :control) 'quit-game))

(defresource "go.wav" :volume 23)

(defmethod add-random-hazard ((arena arena))
  (increase-danger 1.5)
  (multiple-value-bind (x y) (center-point arena)
    (let ((hazard (hazard))
	  (rx (* (random-choose '(-1 1)) (random 20)))
	  (ry (* (random-choose '(-1 1)) (random 20))))
      (when hazard
	(play-sample "error.wav")
	(add-node arena hazard
		  (- x rx (/ (slot-value hazard 'width) 2))
		  (- y ry (/ (slot-value hazard 'height) 2)))))))

(defun drop-player-1 ()
  (add-node (current-buffer) (player-1) (units 11) (units 12))
  (reset-physics (player-1)))

(defun announce (level)
  (play-sample (level-fanfare *level*))
  (play-music "pole.ogg")
  (show-message (format nil "LEVEL ~S GO!" (1+ level))))

(defun make-game (&optional (level 1))
  (with-buffer (make-instance 'arena)
    ;; (xelf::reset-joystick)
    ;; (xelf::scan-for-joysticks)
    (configure-game level)
    (announce level)
    (paste (current-buffer) (make-border 0 0 (- *width* (units 1)) (- *height* (units 1))))
    (trim (current-buffer))
    (multiple-value-bind (x y) (center-point (current-buffer))
      (set-player-1 (make-instance 'robot))
      (drop-player-1)
      (add-node (current-buffer) (make-instance 'core) (- x (/ *minimum-core-size* 2)) (- y (/ *minimum-core-size* 2)))
      (setf (slot-value (current-buffer) 'background-color) *arena-color*)
      (trim (current-buffer)))))

(defun do-reset (&optional (level 0))
  (reset-score)
  (setf *danger* 0)
  (setf *level* level)
  (start (make-game level))
  (reset-game-clock))

(defmethod reset-game ((self arena) &optional (level 0))
  (stop self)
  (dotimes (n 30)
    (halt-sample n))
  (do-reset level)
  (at-next-update (destroy self)))

(defmethod reset-level ((self arena))
  (reset-game self *level*))
  
(defmethod select-variation ((arena arena))
  (let ((v *variation*))
    (incf v)
    (setf v (mod v (length *variations*)))
    (setf *variation* v)
    (reset-game arena)))

(defmethod toggle-music ((self arena))
  (setf *use-music* (if *use-music* nil t))
  (if *use-music*
      (play-rhythm)
      (halt-music)))

(defparameter *debug* nil)

(defmethod toggle-debug ((self arena))
  (setf *debug* (if *debug* nil t)))

;; (defmethod proceed ((arena arena))
;;   (play-sample "go.wav")
;;   (drop-particle)
;;   (drop-player-1)
;;   (clear-goals))

(defparameter *score-font* "sans-mono-bold-12")

(defparameter *big-font* "sans-mono-bold-32")

(defun danger-color (&optional (danger (danger)))
  (cond ((< danger 10) "lawn green")
	((< danger 20) "chartreuse")
	((< danger 30) "yellow")
	((< danger 40) "gold")
	((< danger 50) "orange")
	((< danger 60) "dark orange")
	((< danger 70) (random-choose '("orange" "orange red")))
	((< danger 85) (random-choose '("red" "orange red")))
	(t (random-choose '("yellow" "magenta")))))

(defmethod draw :after ((arena arena))
  (when (stringp *message*)
    (draw-string *message* 35 35 :color "yellow" :font *big-font*))
  (draw-string (game-clock-string) (units 31.6) 3 :color "white" :font *score-font*)
  (draw-string (format nil "SCORE: ~S" *score-1*) (units 52.6) 3 :color "cyan" :font *score-font*)
  (draw-string (format nil "LEVEL: ~S" (1+ *level*))
	       (units 21.6) 3
	       :color "yellow"
	       :font *score-font*)
  (let ((bar-height (* 7 (danger))))
    (draw-box 1263 
	      (- (/ *height* 2) (/ 700 2))
	      12
	      (- *height* 40)
	      :color "gray14")
    (draw-box 3
	      (- (/ *height* 2) (/ 700 2))
	      12
	      (- *height* 40)
	      :color "gray14")
    (draw-box 1263 
	      (- (/ *height* 2) (/ bar-height 2))
	      12
	      bar-height
	      :color (danger-color))
    (draw-box 3
	      (- (/ *height* 2) (/ bar-height 2))
	      12
	      bar-height
	      :color (danger-color))
    (when *use-gravity*
      (draw-string "GRAVITY"
		   (units 3.6) 3
		   :color (random-choose '("cyan" "white"))
		   :font *score-font*)
      (multiple-value-bind (x y) (center-of-arena) 
	(draw-image "gravity.png" 0 0
		    :height *height*
		    :width *width*)))
    (when *use-deathwalls*
      (draw-string "DEATHWALLS"
		   (units 9.6) 3
		   :color (random-choose '("cyan" "white"))
		   :font *score-font*))
    (when *use-plasma*
      (draw-string "PLASMA"
		   (units 6.8) 3
		   :color (random-choose '("yellow" "magenta"))
		   :font *score-font*))
    ;; slot
    (when (plusp *danger-clock*)
      (draw-box (units 41.5) 3 (units 6.5) 15 :color "red"))
    (draw-string (format nil "DANGER: ~,03f %" (danger))
		 (units 41.6) 3
		 :color "yellow"
		 :font *score-font*)
    (draw-string " [Escape] Setup   [Arrows/Numpad/Joystick] move   [SPACE/JFire] drop bomb   [SHIFT or JOther] charge trail   [Control-R] reset game  [Control-Y] Retry level   [Control-Q] quit"
		 (units 0.4) (- *height* 17)
		 :color "white"
		 :font *score-font*)))
  
;;; Preventing mousey movements

(defmethod handle-point-motion ((self arena) x y))
(defmethod press ((self arena) x y &optional button))
(defmethod release ((self arena) x y &optional button))
(defmethod tap ((self arena) x y))
(defmethod alternate-tap ((self arena) x y))

;;; Title screen

(define-buffer title 
  (quadtree-depth :initform 4)
  (background-image :initform "title.png"))

(defmethod initialize-instance :after ((title title) &key)
  (play-music "blob.ogg" :loop t)
  (resize title *nominal-screen-width* *nominal-screen-height*))

(defmethod get-started ((title title))
  (halt-music)
  (at-next-update (switch-to-buffer (make-game *level*))))

(defmethod update :after ((title title))
  (when (keyboard-down-p :space)
    (get-started title)))

(defmethod handle-event :after ((title title) event) 
  (when (eq :joystick (first event))
    (destructuring-bind (which button direction) (rest event)
      (setf *player-1-joystick* which)
      (get-started title))))

(defmethod tap ((title title) x y))
(defmethod scroll-tap ((title title) x y) nil)
(defmethod alternate-tap ((title title) x y) nil)

;;; Game setup screen

(defparameter *button-time* 30)
(defparameter *ready-time* 120)

(defclass setup (xelf:buffer)
  ((timer :initform 0)
   (player :initform 1)
   (quadtree-depth :initform 4)
   (background-color :initform "CornflowerBlue")))

(defmethod initialize-instance :after ((setup setup) &key)
  (resize setup *width* *height*))

(defmethod update :after ((setup setup))
  (with-slots (timer player) setup
    (when (plusp timer) 
      (decf timer))
    (when (and (zerop timer)
	       (null player))
      (stop setup)
      (do-reset))))

(defparameter *p1-prompt* "Press the FIRE button on the controller you want to use.")

(defun ready-prompt ()
  "Player 1 versus the computer. Get ready!")

(defparameter *must* "(Controller must be plugged in before the application is started.)")

(defun flicker () (random-choose '("white" "cyan")))

(defmethod draw :after ((setup setup))
  (draw-image "notice.png" 300 500)
  (with-slots (timer player) setup
    (draw-string "Game setup" (units 28.3) (units 2) :color "white" :font "sans-mono-bold-22")
    (draw-string *must* (units 20) (units 5) :color "white" :font "sans-mono-bold-12")
    (if player 
	(draw-string *p1-prompt* (units 12) (units 12) :color (flicker) :font "sans-mono-bold-18")
	(draw-string (ready-prompt) (units 12) (units 18) :color "white" :font *big-font*))))

(defmethod handle-event ((setup setup) event)
  (with-slots (timer player) setup
    (when (and (eq :joystick (first event))
	       (not (plusp timer)))
      (destructuring-bind (which button direction) (rest event)
	(when player
	  (setf *player-1-joystick* which)
	  (setf *fire-button* button)
	  (setf timer *button-time*)
	  (setf timer *ready-time*)
	  (setf player nil))))))

(defmethod setup ((arena arena))
  (stop arena)
  (at-next-update 
    (switch-to-buffer (make-instance 'setup))
    (destroy arena)))

;;; Main program

(defparameter *title-string* "4x0ng 0.91")

(defun 4x0ng (&optional (level 1))
  (setf *debug* nil)
  (setf *danger* 0)
  (setf *level* (1- level))
  (setf *use-music* t)
  (setf *difficult* nil)
  (setf *use-plasma* nil)
  (setf *use-gravity* nil)
  (setf *variation* 1)
  (setf *fullscreen* nil)
  (setf *font-texture-scale* 2)
  (setf *font-texture-filter* :linear)
  (setf *window-title* *title-string*)
  (setf *screen-width* *width*)
  (setf *screen-height* *height*)
  (setf *nominal-screen-width* *width*)
  (setf *nominal-screen-height* *height*)
  (setf *scale-output-to-window* t) 
  (setf *default-texture-filter* :nearest)
  (setf *use-antialiased-text* nil)
  (setf *frame-rate* 60)
  (disable-key-repeat) 
  (reset-score)
  (reset-game-clock)
  (with-session 
    (open-project "4x0ng")
    (index-all-images)
    (index-all-samples)
    (index-pending-resources)
    (preload-resources)
    (at-next-update (switch-to-buffer (make-instance 'title)))))

;;; 4x0ng.lisp ends here
