4x0ng is a game of quick reactions and think-ahead strategy. An
overheating particle reactor must be shut down. From a remote control
viewing station, you guide unmanned robotic probes that can safely
enter the reactor—one at a time. All nine procedurally-generated
reactor chambers (i.e. game levels) must be shut down in order to
avert nuclear disaster and complete the game.

Particle types don’t mix; each level is divided into up to three
chambers, each with one type of particle reacting in it. If too many
particles build up in any one chamber of the reactor, the reaction
goes out of control, and you lose a life.

Various high-energy particles (alpha, beta, and gamma) are bouncing
through the reactor; after a set number of bounces, a particle splits
into two particles of the same type, both of which are moving a bit
faster, and so on.

If the probe comes into contact with any object or surface, it is
immediately destroyed from heat. If all three available robotic probes
are destroyed, there will be no way to stop the meltdown, and it’s
Game Over.

The probe will be destroyed if it becomes too hot. The heat gauge will
normally decrease slowly on its own, but it can be drained more
quickly at a cooling vent (if any.) Colliding with a particle
increases heat by about 35% of the gauge’s width, meaning that you can
only survive one or two occasional bumps. “Hot zones” are floating
regions of intense temperature, and will increase heat relatively
quickly as long as the player is in contact with them; passing through
these clouds should be nearly a last resort. These clouds can overlap
for additive effect.

Your probe is trailed by a positronic filament (represented by a
yellow line following your probe) designed to capture the bouncing
particles. By sweeping the filament across the path of an oncoming
particle, you can annihilate them and reduce the danger level. The
tail is not overly long (this would make the game too easy) and cannot
be extended. And, the particle still hurts you (with heat) if it
collides with the robot probe (instead of the trail.)

Each level takes up the entire screen; the only status displays are a
row of three squares at the bottom left corner whose coloring
indicates how many robotic arms are left, and a heat gauge. The
central column is present in all levels, and so the center of the
screen is both the entrance and exit point.

The particles move faster than your probe, so you have to watch their
(mostly) deterministic behavior and plan out your moves in advance.

A level is mostly empty space with particles bouncing in it, but
chamber sizes and positions will vary. New particles come periodically
from guns, with one gun located in each chamber. The player must move
dynamically between different chambers, avoiding walls and guns, in
order to keep grabbing particles and preventing any chamber from going
critical. If the player survives 2 minutes of particles without a
meltdown, the level is completed, and the reactor doors begin (very
slowly) closing as the level shuts down and an alarm sounds. The
player must race to the exit (in the center of the screen) to move on
to the next level.
